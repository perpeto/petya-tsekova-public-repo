package pages.forum;

import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;

import static com.telerikacademy.testframework.Utils.getUIMappingByKey;

public class MainTopicsPage extends BaseForumPage{

    public MainTopicsPage(WebDriver driver, String pageUrlKey) {
        super(driver, pageUrlKey);
    }

    public void createNewTopic() throws InterruptedException {

        String topicName = getUIMappingByKey("forum.newTopicName");
        String topicText = getUIMappingByKey("forum.newTopicText");
        String hyperlink = getUIMappingByKey("forum.hyperlink");

        LocalDateTime timestamp = LocalDateTime.now();

        actions.waitForElementClickable("forum.header.create.button");
        actions.clickElement("forum.header.create.button");

        actions.waitForElementVisible("forum.create.titleInput");
        actions.typeValueInField(topicName + timestamp, "forum.create.titleInput");

        actions.waitForElementVisible("forum.create.categoryButton");
        actions.clickElement("forum.create.categoryButton");
        actions.waitForElementClickable("forum.create.categoryButtonDropDown");
        actions.clickElement("forum.create.categoryButtonDropDown");

        actions.waitForElementClickable("forum.create.tagsButton");
        actions.clickElement("forum.create.tagsButton");
        actions.waitForElementClickable("forum.create.tagsButtonDropDown");
        actions.clickElement("forum.create.tagsButtonDropDown");

        actions.waitForElementVisible("forum.create.textInput");
        actions.typeValueInField(topicText + timestamp, "forum.create.textInput");

        //hyperlink
        actions.waitForElementClickable("forum.create.hyperlinkButton");
        actions.clickElement("forum.create.hyperlinkButton");

        actions.waitForElementVisible("forum.create.hyperlinkInput");
        actions.typeValueInField(hyperlink, "forum.create.hyperlinkInput");
        actions.waitForElementClickable("forum.create.hyperlinkOkButton");
        actions.clickElement("forum.create.hyperlinkOkButton");

        actions.clickElement("forum.create.createTopicFinalButton");

    }

    public void assertIfNewTopicPresents(String locator) {
        actions.assertElementPresent(locator);
    }


    public void createNewComment() throws InterruptedException {

        String commentInput = getUIMappingByKey("forum.textReply");


        actions.waitForElementClickable("forum.newComment.moreButton");
        actions.clickElement("forum.newComment.moreButton");
        actions.waitForElementClickable("forum.newComment.themeOfDidi");
        actions.clickElement("forum.newComment.themeOfDidi");

        Thread.sleep(10000);
        actions.waitForElementClickable("forum.newComment.replyButton");
        actions.clickElement("forum.newComment.replyButton");

        //comment input
        actions.waitForElementVisible("forum.newComment.replyInput");
        actions.typeValueInField(commentInput, "forum.newComment.replyInput");
        actions.waitForElementClickable("forum.newComment.replyButtonFinal");
        actions.clickElement("forum.newComment.replyButtonFinal");

    }

    public void assertIfNewReplyPresents(String locator) {
        actions.assertElementPresent(locator);
    }

    public void likeTopic() throws InterruptedException {

        actions.waitForElementClickable("forum.newComment.someTheme");
        actions.clickElement("forum.newComment.someTheme");

        actions.waitForElementClickable("forum.like.heartElement");
        actions.clickElement("forum.like.heartElement");

    }
    public void assertIfLiked(String locator) {

        actions.assertElementPresent(locator);
    }

    public void unlikeTopic() throws InterruptedException {

        actions.waitForElementClickable("forum.newComment.someTheme");
        actions.clickElement("forum.newComment.someTheme");

        actions.waitForElementClickable("forum.like.heartElement");
        actions.clickElement("forum.like.heartElement");

    }
    public void assertIfUnliked(String locator) {

        actions.assertElementPresent(locator);
    }
}
