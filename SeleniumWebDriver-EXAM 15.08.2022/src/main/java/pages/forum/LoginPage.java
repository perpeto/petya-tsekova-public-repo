package pages.forum;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseForumPage {

    public static String boardId = null;

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser(String userKey) {
        String username = getConfigPropertyByKey("email");
        String password = getConfigPropertyByKey("password");

        navigateToPage();
        assertNavigatedUrl();
        //кликваме върху бутон login
        actions.waitForElementVisible("forum.loginPage.login");
        actions.clickElement("forum.loginPage.login");

        actions.waitForElementVisible("forum.loginPage.email");

        //попълваме мейл и парола и натискаме login
        actions.typeValueInField(username, "forum.loginPage.email");
        actions.typeValueInField(password, "forum.loginPage.password");
        actions.clickElement("forum.loginPage.loginButton");

//        actions.waitForElementVisible("forum.header.create.button"); //проверка
    }
}
