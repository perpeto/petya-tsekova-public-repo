package test.cases;

import com.sun.tools.javac.Main;
import org.junit.Test;
import pages.forum.MainTopicsPage;

public class ForumTest extends BaseTestSetup{

    @Test
    public void createNewTopic() throws InterruptedException {
        login();

        MainTopicsPage mainTopicsPage = new MainTopicsPage(actions.getDriver(), "home.page");
        mainTopicsPage.createNewTopic();

        mainTopicsPage.assertIfNewTopicPresents("forum.create.assertNewTopic");
    }

    @Test
    public void createNewComment() throws InterruptedException {
        login();

        MainTopicsPage mainTopicsPage = new MainTopicsPage(actions.getDriver(), "home.page");
        mainTopicsPage.createNewComment();

        mainTopicsPage.assertIfNewReplyPresents("forum.create.assertReply");
    }

    @Test
    public void likeTopic() throws InterruptedException {
        login();

        MainTopicsPage mainTopicsPage = new MainTopicsPage(actions.getDriver(), "home.page");
        mainTopicsPage.likeTopic();

        mainTopicsPage.assertIfLiked("forum.like.assertLike");
    }

    @Test
    public void unlikeTopic() throws InterruptedException {
        login();

        MainTopicsPage mainTopicsPage = new MainTopicsPage(actions.getDriver(), "home.page");
        mainTopicsPage.unlikeTopic();

        mainTopicsPage.assertIfUnliked("forum.like.assertUnlike");
    }
}
