import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class TelerikAcademyBingAndGoogleTest extends Constants {

    @Test
    public void navigateToTelerikBingSearch() {
        //ARRANGE
        System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        //ACT
        webDriver.navigate().to("https://www.bing.com/");

        WebElement searchBar = webDriver.findElement(By.id("sb_form_q"));
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.sendKeys(Keys.RETURN);
        webDriver.findElement(By.id("bnp_btn_accept")).click();

        String inputSearch = webDriver.findElement(By.xpath("(//a[@href='https://www.telerikacademy.com/alpha'])[1]")).getText();

        //ASSERT
        Assert.assertEquals("wrong text!", SEARCH_STRING, inputSearch);

        webDriver.quit();

    }

    @Test
    public void navigateToTelerikGoogleSearch() {
        //ARRANGE
        System.setProperty("webdriver.chrome.driver", PATH_TO_DRIVER);

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        //ACT
        webDriver.navigate().to("https://www.google.com/");
        webDriver.findElement(By.xpath("(//div[@class='QS5gu sy4vM'])[2]")).click();


        WebElement searchBar = webDriver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.sendKeys(Keys.RETURN);

        String inputSearch = webDriver.findElement(
                By.xpath("(//h3[@class='LC20lb MBeuO DKV0Md'])[1]")).getText();

        //ASSERT
        Assert.assertEquals(SEARCH_STRING, inputSearch);

        if (inputSearch.equals(EXPECTED_STRING))
            System.out.println("Title is correct");
        else
            System.out.println("Title is not correct");


        webDriver.quit();

    }
}
